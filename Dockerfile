FROM ubuntu:21.04

ARG DEBIAN_FRONTEND=noninteractive

#RUN apt-get -y --fix-missing update \
RUN apt-get -y update \
 && apt-get -y install apt-utils \
 && apt-get -y upgrade
#RUN apt-get -y --fix-missing update
RUN apt-get -y install software-properties-common # python-software-properties
RUN add-apt-repository -y ppa:ethereum/ethereum
#RUN apt-get -y update
#RUN apt-get -y install ethereum
#RUN apt-get -y install golang \
RUN apt-get -y install \
    python3-pip python3-cytoolz \
    git telnet curl wget tree emacs-nox sudo psmisc xz-utils

RUN curl -L https://github.com/ethereum/solidity/releases/download/v0.7.6/solc-static-linux >bin/solc-0.7.6
RUN chmod +x bin/solc*

#RUN apt-get install -y protobuf-compiler
#RUN cp /usr/bin/solc-0.7.6 /usr/bin/solc

#RUN git clone https://github.com/ethereum/go-ethereum.git
#RUN cd go-ethereum ; make geth
#RUN cp go-ethereum/build/bin/geth /usr/bin

RUN pip3 install docopt web3==5.28 eth_account

RUN cd /usr/local ; wget https://nodejs.org/dist/v16.14.2/node-v16.14.2-linux-x64.tar.xz
RUN cd /usr/local ; tar xvf node-v16.14.2-linux-x64.tar.xz
RUN cd /usr/local ; ln -s node-v16.14.2-linux-x64 node

RUN cd /usr/local/bin ; ln -sf /usr/local/node/bin/* .

RUN npm i -g ganache npm@8.7

RUN cd /usr/local/bin ; ln -sf /usr/local/node/bin/* .

#RUN apt-get -y install npm
#RUN npm i -g ganache-cli
#RUN apt-get -y install npm && npm install -g ganache-cli

#RUN apt-get -y install software-properties-common # python-software-properties

#RUN add-apt-repository -y ppa:ethereum/ethereum
#RUN apt-get -y update
#RUN apt-get -y install ethereum

#RUN apt-get -y install git telnet curl wget tree emacs-nox

#RUN apt-get -y update && apt-get -y upgrade && apt-get -y install apt-utils
#RUN apt-get -y update && apt-get -y upgrade && apt-get -y install apt-utils
#RUN apt-get -y install curl xz-utils telnet tree python3-pip npm wget cmake libboost-all-dev sudo
#RUN apt-get -y install curl telnet tree wget sudo
#RUN apt-get -y install curl telnet tree python3-pip npm wget
#RUN apt-get -y install wget curl telnet tree python3-pip python3-cytoolz
#RUN apt-get -y install python3-pip python3-cytoolz
#RUN wget https://github.com/ethereum/solidity/releases/download/v0.7.6/solidity_0.7.6.tar.gz
#RUN tar -xvzf solidity_0.7.6.tar.gz
#RUN ./solidity_0.7.6/scripts/build.sh
#RUN rm -fr solidity_0.7.6*
#RUN ln -s /usr/local/bin/solc /usr/local/bin/solc-0.7.6
#RUN curl -L https://github.com/ethereum/solidity/releases/download/v0.8.6/solc-static-linux >bin/solc
#RUN curl -L https://github.com/ethereum/solidity/releases/download/v0.4.25/solc-static-linux >bin/solc-0.4.25
#RUN pip3 install docopt web3==5.24 eth_account
#RUN pip3 install ipython jupyter gevent-websocket bottle
#RUN npm i -g hardhat
#RUN apt-get -y install emacs-nox screen

#RUN apt-get -y install git

#RUN apt-get -y install git npm && npm install -g ganache-cli@beta

#ADD lordkelvin/scripts/download.py ./
#RUN ./download.py https://github.com/ethereum/solidity/releases/download/v0.7.6/solc-static-linux -x /usr/local/bin/solc-0.7.6

RUN wget https://gist.github.com/val314159/acf63425eb5c6e6757df225d27c09d62/raw/ca4c3e1122b7ef88217f4fdedd694d6d4cbd9d21/geth
RUN chmod +x geth ; mv geth /usr/bin
#ADD geth /usr/bin


ADD requirements.txt lordkelvin/tools/download.sh ./
#RUN pip3 install docopt
RUN (mkdir 3rd ; cd 3rd ; sh /download.sh) && rm download.sh
RUN pip3 install -r requirements.txt ; rm requirements.txt

#RUN npm i -g yarn @opengsn/cli @opengsn/contracts @opengsn/provider @truffle/hdwallet-provider

#RUN ln -s /root/3rd /3rd

WORKDIR /root/

ADD .  lordkelvin/
#ADD setup.py requirements.txt README.md MANIFEST.in lk/
#ADD lordkelvin/ lk/lordkelvin/
RUN pip3 install ./lordkelvin
RUN cp  lordkelvin/lordkelvin/tools/aliases /root/.bash_aliases
#RUN pip3 install ./lordkelvin

#RUN cp lordkelvin/lk /usr/local/lib/python3.9/dist-packages/lordkelvin/__init__.py

#RUN mkdir @uniswap @openzeppelin @lordkelvin
#RUN ln -s /3rd/v3-* @uniswap
#RUN ln -s /3rd/openzeppelin-contracts/contracts @openzeppelin
#RUN ln -s /3rd/openzeppelin-contracts-upgradeable/contracts @openzeppelin/contracts-upgradea
#RUN ln -s /3rd/openzeppelin-contracts-upgradeable/contracts @openzeppelin/contracts-upgradea
#RUN ln -s ../lordkelvin/lordkelvin/contracts @lordkelvin/contracts

#RUN ln -s /3rd /root/@lordkelvin/contracts
#RUN pip3 install ./lordkelvin && rm -fr lordkelvin
#WORKDIR /root
#ADD tools/aliases /root/.bash_aliases
EXPOSE 8545 9545 10545 11545 8888 8000 8080 80

#RUN apt-get -y install git telnet curl wget tree emacs-nox

#WORKDIR /usr/local/lk/
##RUN apt-get -y install npm && npm install -g ganache-cli
#ADD setup.py requirements.txt README.md MANIFEST.in lordkelvin/
#ADD lordkelvin/ lordkelvin/lordkelvin/
##ADD . lordkelvin/
#RUN pip3 install ./lordkelvin && rm -fr lordkelvin
##RUN download.py https://gist.github.com/val314159/70f0552435829a497a4d820b01f7026d/raw/0b09a6a905224ee6a7a2ac3fca5a541dae47c475/solc /usr/local/bin/solc-0.7.6
#WORKDIR /root
#ADD tools/aliases /root/.bash_aliases
#EXPOSE 8545 9545 10545 11545 8888 8000 8080 80
