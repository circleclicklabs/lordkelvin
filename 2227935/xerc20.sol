// SPDX-License-Identifier: MIT
pragma solidity >=0.7.6;
contract xerc20 {
    string  public name;
    string  public symbol;
    uint8   public decimals;
    uint256 public totalSupply;
    mapping(address =>                    uint256)  public balanceOf;
    mapping(address => mapping(address => uint256)) public allowance;
    event Approval(address indexed owner, address indexed spendr, uint256 amt);
    event Transfer(address indexed from,  address indexed to,     uint256 amt);
    function approve(address _spender, uint256 _amount) public returns(bool) {
        allowance    [msg.sender][_spender]  = _amount;
        emit Approval(msg.sender, _spender,    _amount);
        return true;}
    function _transferFrom(address _from,
			   address _to, uint256 _amount)internal returns(bool){
	balanceOf[_from] -= _amount;
	balanceOf[_to  ] += _amount;
	emit Transfer(_from, _to, _amount);
	return true;}
    function transfer(    address _to, uint256 _amount) public returns(bool b){
        if ( ( balanceOf[msg.sender]        >= _amount) )
	    return _transferFrom(msg.sender, _to, _amount);}
    function transferFrom(address _from,
			  address _to, uint256 _amount) public returns(bool b){
        if ( ( balanceOf[_from]             >= _amount &&
	       allowance[_from][msg.sender] >= _amount &&
	       balanceOf[_to] + _amount > balanceOf[_to] ) ) {
	    allowance[_from][msg.sender] -= _amount;
	    return _transferFrom(_from,      _to, _amount);}}
    function initialize(string memory _name, string memory _symbol,
			uint8 _decimals, uint256 _totalSupply) public {
        (name, symbol, decimals) = (_name, _symbol, _decimals);
        balanceOf[msg.sender] = totalSupply = _totalSupply;
        emit Transfer(address(0), msg.sender, _totalSupply);}}
