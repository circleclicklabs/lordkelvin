#!/usr/bin/env python3
import os, sys, json
if sys.argv[1] == 'artifact2out':
    try:
        pfx, name = sys.argv[2:]
        home = os.getenv('HOME')
        f = open(f'{home}/node_modules/{pfx}/artifacts/contracts/{name}.sol/{name}.json')
    except ValueError:
        f = sys.stdin
        pass
    j = json.load(f)
    name = j['contractName']
    try: os.mkdir('out')
    except FileExistsError: pass
    with open(f'out/{name}.abi','w') as f:
        json.dump(j['abi'], f)
        pass
    with open(f'out/{name}.bin','w') as f:
        f.write(j['bytecode'])
        pass
elif sys.argv[1] == 'flip':
    fn0, fn1 = sys.argv[2:]
    fn0 = f'out/{fn0}.cta'
    fn1 = f'out/{fn1}.cta'
    t0 = open(fn0).read().upper()
    t1 = open(fn1).read().upper()
    if t0 >= t1:
        tmp = 'tmp'
        os.replace(fn0, tmp)
        os.replace(fn1, fn0)
        os.replace(tmp, fn1)
        pass
    pass
else:
    print("WTF")
    exit(1)
    
    
