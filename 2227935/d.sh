#!/bin/bash
set -e
#set -ex
export PROFILEDB=profiledb
PATH=$PATH:./out
function b {
    echo `T0 balanceOf @@S`     `T1 balanceOf @@S` // \
	 `T0 balanceOf $PUBLIC` `T1 balanceOf $PUBLIC`
}
#lk.py deploy UniswapV3Factory
#lk.py deploy Unicycle         --as S 1 2 3 4
./lk.py deploy UnicycleMax      --as S
./lk.py deploy UniswapV3Factory --as F
./lk.py deploy xerc20           --as T0
./lk.py deploy xerc20           --as T1
./tool.py flip T0 T1
T0 initialize "Token Zero" TOK0 18 12000000000000000000
T1 initialize "Token One"  TOK1 18 12000000000000000000
F createPool @@T0 @@T1 500
F    getPool @@T0 @@T1 500 | save UniswapV3Pool --as P -
S x96 ~10 | P initialize -
P slot0
T0 approve  @@P 999999999
T1 approve  @@P 999999999
T0 approve  @@S 999999999
T1 approve  @@S 999999999
T0 transfer @@S 999999999
T1 transfer @@S 999999999
P tickSpacing

echo BAL `T0 balanceOf $PUBLIC` `T1 balanceOf $PUBLIC`

S mint @@P @@S  ~2010 ~2000       0 100000002
S mint @@P @@S   2000       100000001       0

S mint @@P @@S     20 30 250000 250000
S mint @@P $PUBLIC 10 20 250000 250000

S pos  @@P $PUBLIC 10 20
S pos  @@P @@S     20 30

S swap @@P @@S      300000
S swap @@P @@S     ~600000
S swap @@P @@S      600000
S swap @@P $PUBLIC ~100000

S mint @@P @@S     20 30 1 1
S mint @@P $PUBLIC 10 20 1 1

S pos  @@P $PUBLIC 10 20
S pos  @@P @@S     20 30

echo BAL `T0 balanceOf $PUBLIC` `T1 balanceOf $PUBLIC`

P burn             10 20 0
S burn @@P         20 30 0

echo BAL `T0 balanceOf $PUBLIC` `T1 balanceOf $PUBLIC`

S pos  @@P $PUBLIC 10 20
S pos  @@P @@S     20 30

echo BAL `T0 balanceOf $PUBLIC` `T1 balanceOf $PUBLIC`

S burn @@P         20 30 500650385

echo BAL `T0 balanceOf $PUBLIC` `T1 balanceOf $PUBLIC`

S pos  @@P @@S 20 30

exit 0
S computePosKey $PUBLIC 10 20
P positions `S computePosKey $PUBLIC 10 20`
S pos       @@P $PUBLIC 10 20

#S mint @@P @@P  2005 1001    0
#S mint @@P @@P ~2005    0 1002
S mint @@P @@P ~2010 ~2000   0 1002
S mint @@P @@P  2000 1001    0
b
S swap @@P @@S 500
b
S swap @@P @@S ~100
b
S swap @@P $PUBLIC ~100
b
