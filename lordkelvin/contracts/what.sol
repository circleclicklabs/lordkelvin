// SPDX-License-Identifier: MIT
pragma solidity >=0.7.6;

pragma abicoder v2;

struct ABC {

    int A;

    int B;

    int C;

}

contract what {

    ABC public abc;
    
    function xzz(ABC calldata x) external {
	abc.A = x.A;
	abc.B = x.B;
	abc.C = x.C;
    }

    function zz() external view returns(ABC memory z) {
	this;
	z.A = 2;
	z.B = 4;
	z.C = 6;
    }
    
    function xyz(address[] calldata a) external view returns(address[] calldata) {
	this;
	return a;
    }
    
    function wxyz(uint[] calldata a) external view returns(uint[] calldata) {
	this;
	return a;
    }
    
}
