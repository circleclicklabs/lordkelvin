#include tools/tools.mk

WETH9=0xd0A1E359811322d97991E03f863a0C30C2cF029C
USDC=0xe22da380ee6B445bb8273C81944ADEB6E8450422
DAI=0x4F96Fe3b7A6Cf9725f59d353F723c1bDb64CA6Aa

#SOLC:=docker run -w`pwd` -v`pwd`:`pwd` solc -oout --optimize
SOLC:=solc-0.7.6 -oout --optimize --no-color --abi --bin --overwrite --allow-paths=.

call: clean all
all: abis t1
#all: abis test
clean:
	rm -fr *.log out *.egg-info dist build *.tmp ?
	find . -name \#* -o -name .\#* | xargs rm -fr
	find . -name \*~ -o -name .\*~ | xargs rm -fr
	find . -name  __pycache__      | xargs rm -fr
	tree -I '@3rd' .
realclean: kg clean
	rm -fr *.prv *.pub *.pid 3rd .tox activate.sh
abis: lordkelvin/contracts/3rd
	cd lordkelvin/contracts ; $(SOLC) *.sol \
  @uniswap/v3-core/contracts/UniswapV3Factory.sol \
  @uniswap/v3-periphery/contracts/SwapRouter.sol
	ln -s lordkelvin/contracts/out
#	$(SOLC) @uniswap/swap-router-contracts/contracts/*.sol
interfaces:
	$(SOLC) @uniswap/v3-*/contracts/interfaces/*.sol
	find @openzeppelin/contracts -name I\*.sol | xargs $(SOLC) contracts/*.sol
t1:
	scripts/env.sh make t2
t2: x1 x2
x2:
	lk x y slot0
x1:
	echo 1100000000 | \
	lk d erc20 --as t1 token1 TOK1 18 -
	lk d erc20 --as t2 -- token2 TOK2 18 1200000000
	lk d erc20 --as t3 token3 TOK3 18 1300000000
	lk d UniswapV3Factory --as f
	lk d SwapRouter       --as s @@f @@f
	lk d Utils --as u        @@s @@f @@f
	lk x u -a
	lk x u price -- -1
	out/u price -- -1
	out/u -a
	lk x u mkPool @@t1 @@t2  500 `lk x u price 0`
	lk x u   pool @@t1 @@t2  500 >x
	lk s UniswapV3Pool --as y `cat x`
	rm x
simple_deploy:
	lk d erc20 --as t1 token1 TOK1 18 1100000000
	out/t1 -a
	out/t1 -b
	balance @@t1
	out/t1 balanceOf ${PUBLIC}
qqqqqqqqq:
	lk c u.price ~1
	lk c u.price  0
	lk c u.price  1
	lk c u.price  887272
	lk c u.price ~887270
	lk c u.price ~887271
	lk c u.price ~887272
	lk x u.mkPool @@t1 @@t2 500 `lk c u price 0`
test:
	save      erc20 --as WETH9 ${WETH9}
	save      erc20 --as USDC  ${USDC}
	save      erc20 --as DAI   ${DAI}
	save     IWETH9            ${WETH9}
	out/IWETH9 -a
	out/WETH9  -a
	out/USDC   -a
	out/DAI    -a
	out/WETH9  balanceOf ${PUBLIC}
	out/USDC   balanceOf ${PUBLIC}
	out/DAI    balanceOf ${PUBLIC}
	deploy    erc20 --as token1 token1 TOK1 18 1000000000
	out/token1 balanceOf ${PUBLIC}

PORT?=18888

gg:
	scripts/launch_ganache.py ${PORT}

ggsh:
	scripts/launch_ganache.py ${PORT} "bash -l"

sh:
	bash -l

g:
	scripts/lg.py ${PORT} 'scripts/env.sh make all'

ge:
	scripts/launch_ganache.py ${PORT} 'scripts/env.sh make all'

ges:
	scripts/launch_ganache.py ${PORT} 'scripts/env.sh make all sh'

kg:
	launch_ganache.py -k

ganache.pid:
	scripts/launch_ganache.py ${PORT}

dd: docker

VM?=lk

dbuild: 3rd
	docker build -f tools/Dockerfile.`uname` . -t lk

exec:
	docker exec     -it -w`pwd` ${VM} bash -l

docker: dbuild
	docker run --rm -it -w`pwd` -v`pwd`/..':'`pwd`/.. \
		-e PORT=${PORT} \
		--network=host --name ${VM} lk

runges: dbuild
	docker run --rm -it -w`pwd` -v`pwd`/..':'`pwd`/.. \
		-e PORT=${PORT} \
		--network=host --name ${VM} lk make ges

runge: dbuild
	docker run --rm -it -w`pwd` -v`pwd`/..':'`pwd`/.. \
		-e PORT=${PORT} \
		--network=host --name ${VM} lk make ge

image:
	docker build . -t lkdev

push: image
	docker tag lkdev val314159/lkdev
	docker push val314159/lkdev

3rd:
	mkdir 3rd
	cd 3rd ; sh ../lordkelvin/tools/download.sh

lordkelvin/contracts/3rd:
	mkdir -p $@
	cd $@ ; sh ../../tools/download.sh

contracts::
	cd $@ ; $(SOLC) *.sol \
  @uniswap/v3-core/contracts/UniswapV3Factory.sol \
  @uniswap/v3-periphery/contracts/SwapRouter.sol

basevm::
	docker buildx build . -f tools/Dockerfile.$@ -t val314159/$@ --push --platform linux/amd64,linux/arm64

lk::
	docker buildx build . -t val314159/$@ --push --platform linux/amd64,linux/arm64

solc::
	docker build - -t $@ <tools/Dockerfile.solc-0.7.6

