import os, glob, lordkelvin as lk, setuptools as _
_.setup(name='lordkelvin', version=lk.__version__,
#        use_scm_version=True,
#        setup_requires=['setuptools_scm'],
        description='super cool toolkit',
        long_description=open("README.md").read(),
        long_description_content_type='text/markdown',
        url='https://gitlab.com/circleclicklabs/lordkelvin.git',
        author='Joel Ward', author_email='jmward+python@gmail.com',
        zip_safe=True,
        license='MIT',
        packages=['lordkelvin'],
        include_package_data=True,
        package_data={
            'lordkelvin':["contracts/*.sol", "tools/*"],
        },
        entry_points=dict(console_scripts=['lordkelvin=lordkelvin:main',
                                           'lk=lordkelvin:main',
                                           'deploy=lordkelvin:main2',
                                           'execute=lordkelvin:main2',
                                           'save=lordkelvin:main2',
                                           'balance=lordkelvin:main2',
                                           ]),
        scripts=glob.glob('lordkelvin/scripts/*'),
        install_requires=open('requirements.txt').read().split(),
        classifiers=[
            "Programming Language :: Python :: 3",
            "License :: OSI Approved :: MIT License",
            "Operating System :: OS Independent",
        ])
